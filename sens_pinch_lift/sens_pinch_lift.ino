#include <Servo.h>

Servo lift;  // create servo object to control a servo
Servo pinch;
volatile boolean flag = false;
// twelve servo objects can be created on most boards

void pick_ISR(){
  flag = true;
}

void setup() {
  lift.attach(10);
  pinch.attach(9);
  pinch.write(30);
  lift.write(0);
  attachInterrupt(digitalPinToInterrupt(2), pick_ISR, RISING);
}

void loop() {
  if(flag){
    pinch.write(10);
    lift.write(120);
    delay(1000);
    pinch.write(30);
    delay(500);
    lift.write(0);
    flag = false;
  }
  delay(20);
}
