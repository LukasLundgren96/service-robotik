#include <Servo.h>

Servo leftservo;
Servo rightservo;

void setup() {
  leftservo.attach(10); 
  rightservo.attach(11);
}

void loop() {
  leftservo.write(180);
  rightservo.write(0);
  delay(4000);
}
