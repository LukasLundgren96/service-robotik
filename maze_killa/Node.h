#ifndef NODE_H
#define NODE_H
class Node{
  public:
  Node* parent;
  Junction* junction;
  int action;

 /* Node(){
    parent = NULL;
    junction = NULL;
    action = 0;
  }*/
  
  Node(Node* p, Junction* j, int a){
    action = a;
    junction = j;
    parent = p;
  }
};
#endif
