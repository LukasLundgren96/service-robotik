#include "Junction.h"
#include "Stack.h"

class Maze
{
public:
    Junction* startPos = new Junction();
    Junction* p31 = new Junction();
    Junction* p32 = new Junction();
    Junction* p22 = new Junction();
    Junction* p21 = new Junction();
    Junction* p11 = new Junction();
    Junction* p12 = new Junction();
    Junction* p17 = new Junction();
    Junction* p27 = new Junction();
    Junction* p26 = new Junction();
    Junction* p36 = new Junction();
    Junction* p37 = new Junction();
    Junction* p25 = new Junction();
    Junction* p35 = new Junction();
    Junction* p34 = new Junction();
    Junction* p54 = new Junction();
    Junction* p55 = new Junction();
    Junction* p45 = new Junction();
    Junction* p47 = new Junction();
    Junction* p57 = new Junction();
    Junction* p56 = new Junction();
    Junction* p76 = new Junction();
    Junction* p77 = new Junction();
    Junction* p67 = new Junction();
    Junction* p42 = new Junction();
    Junction* p43 = new Junction();
    Junction* p33 = new Junction();
    Junction* p63 = new Junction();
    Junction* p62 = new Junction();
    Junction* p52 = new Junction();
    Junction* p51 = new Junction();
    Junction* p71 = new Junction();
    Junction* p74 = new Junction();
    Junction* p64 = new Junction();
    Junction* p65 = new Junction();
    Junction* p75 = new Junction();

    Junction* p46 = new Junction();

  Maze()
  {
    

    startPos->setJunctions(nullptr, p31, nullptr, nullptr);
    p31->setJunctions(startPos, p32, nullptr, nullptr);
    p32->setJunctions(p42, nullptr, p22, p31);
    p22->setJunctions(p32, p25, p12, p21);
    p21->setJunctions(nullptr, p22, p11, nullptr);
    p11->setJunctions(p21, nullptr, nullptr, nullptr);
    p12->setJunctions(p22, p17, nullptr, nullptr);
    p17->setJunctions(p27, nullptr, nullptr, p12);
    p27->setJunctions(nullptr, nullptr, p17, p26);
    p26->setJunctions(p36, p27, nullptr, nullptr);
    p36->setJunctions(p46, p37, p26, nullptr);
    p37->setJunctions(nullptr, nullptr, nullptr, p36);
    p25->setJunctions(p25, nullptr, nullptr, p22);
    p35->setJunctions(nullptr, nullptr, p25, p34);
    p34->setJunctions(p54, p35, nullptr, nullptr);
    p54->setJunctions(p64, p55, p34, nullptr);
    p55->setJunctions(nullptr, p56, p45, p54);
    p56->setJunctions(p76, nullptr, nullptr, p55);
    p76->setJunctions(nullptr, p77, p56, nullptr);
    p77->setJunctions(nullptr, nullptr, p67, p76);
    p67->setJunctions(p77, nullptr, nullptr, nullptr);
    p45->setJunctions(p55, p47, nullptr, nullptr);
    p47->setJunctions(p57, nullptr, nullptr, p45);
    p57->setJunctions(nullptr, nullptr, p47, nullptr);
    p42->setJunctions(nullptr, p43, p32, nullptr);
    p43->setJunctions(p63, nullptr, p33, p42);
    p33->setJunctions(p43, nullptr, nullptr, nullptr);
    p63->setJunctions(nullptr, nullptr, p43, p62);
    p62->setJunctions(nullptr, p63, p52, nullptr);
    p52->setJunctions(p62, nullptr, nullptr, p51);
    p51->setJunctions(p71, p52, nullptr, nullptr);
    p71->setJunctions(nullptr, p74, p51, nullptr);
    p74->setJunctions(nullptr, nullptr, p64, p71);
    p64->setJunctions(p74, p65, p54, nullptr);
    p65->setJunctions(p75, nullptr, nullptr, p64);
    p75->setJunctions(nullptr, nullptr, p65, nullptr);

    p46->setJunctions(nullptr, p47, p36, p45);
  }
};
