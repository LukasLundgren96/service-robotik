#ifndef JUNCTION_H
#define JUNCTION_H
class Junction{
public:
  Junction* eastConnection;
  Junction* northConnection;
  Junction* westConnection;
  Junction* southConnection;


  Junction(Junction* ec, Junction* nc, Junction* wc, Junction* sc){
    eastConnection = ec;
    northConnection = nc;
    westConnection = wc;
    southConnection = sc;
  }
  Junction (){
    eastConnection = nullptr;
    northConnection = nullptr;
    westConnection = nullptr;
    southConnection = nullptr;
  }
  void setJunctions(Junction* ec, Junction* nc, Junction* wc, Junction* sc){
    eastConnection = ec;
    northConnection = nc;
    westConnection = wc;
    southConnection = sc;
  }
  Junction getEastJunction(){
    return *eastConnection;
  }
  Junction getNorthJunction(){
    return *northConnection;
  }
  
  Junction getWestJunction(){
    return *westConnection;
  }

  Junction getSouthJunction(){
    return *southConnection;
  }
};
#endif
