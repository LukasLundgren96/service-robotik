#include "Maze.h"
#include "losing_line_2.h"
class AvailableDirections{
public:
  static int const EAST = 0;
  static int const NORTH = 3;
  static int const WEST = 2;
  static int const SOUTH = 1;

  
  static int const LEFT = 1;
  static int const RIGHT = 2;
  static int const FORWARD = 3;
  static int const BACK = -1;
};
class Robot{
private:
  int const EAST = 0;
  int const NORTH = 3;
  int const WEST = 2;
  int const SOUTH = 1;
  int currDir = WEST;
  Junction* knownPos1;
  Junction* knownPos2;
  Junction* blind1;
  Junction* blind2;
  Maze* maze;
  Junction* currJunction;
public:
  Robot(Junction* pos1, Junction* pos2){
    knownPos1 = pos1;
    knownPos2 = pos2;
    maze = new Maze();
    blind1 = maze->p12;
    blind2 = maze->p52;
    currJunction = maze->startPos;

  }

  void solveAndExit(){
    //explore(); make a stack with explore directions and add functionality to fetch closest cylinder first
    if (knownPos1==NULL){
      if(knownPos2!=NULL){
        fetch(knownPos2);
      }
    }else if(knownPos2!=NULL){
      fetch(knownPos1);
      fetch(knownPos2);
    }
    exitMaze();
  }

  void fetch(Junction* goal){
    intStack path = shortestPath(goal);
    while(!path.isEmpty()){
      locomote(path.pop());
    }
  }
  
  void explore(intStack dirs){
    while(!dirs.isEmpty()){//I need some kinda bool when picking up
      //if picked up and not a known pos exit; if known pos make that pos null
      locomote(dirs.pop());
    }
  }

  //search for the lost boi
  intStack shortestPath(Junction* goal){
    Node* node = new Node(NULL,currJunction, 0);// first pointer is special, null parent - null junction - zero action
    // might need to change this into currentPosition to be able to use this method everywhere on the map
    /*node->parent = nullptr;
    node->action = 0;*/

    Queue* frontier = new Queue();
    addChildren(frontier, node);

    while(true){
      if (frontier->isEmpty()){
        intStack* stub = new intStack();
        return *stub;
      }
      
      //may need to deconstruct prev node
      node = frontier->pop();
      
      if(goal == node->junction){
        intStack* answer = new intStack();
        //may need to deconstruct frontier
        while(node->action!=0){
          answer->push(node->action);
          //may need to deconstruct node
          node = node->parent;          
        }
        return *answer;
      }
      else{
        addChildren(frontier, node);
      }
    }
  }
  void addChildren(Queue* fronteir, Node* node){
    if(node->junction->westConnection!=nullptr && node->action!=EAST){//is this correct way of checking for null
      Node* child = new Node(node, node->junction->westConnection, WEST);
      fronteir->push(child);
    }
    if(node->junction->northConnection!=nullptr && node->action!=SOUTH){
      Node* child = new Node(node, node->junction->northConnection, NORTH);
      fronteir->push(child);
    }
    if(node->junction->eastConnection!=nullptr && node->action!=WEST){
      Node* child = new Node(node, node->junction->eastConnection, EAST);
      fronteir->push(child);
    }
    if(node->junction->southConnection!=nullptr&&node->action!=NORTH){
      Node* child = new Node(node, node->junction->southConnection, SOUTH);
      fronteir->push(child);
    }
  }
  /*private:
  bool isVisited(Junction element){// might need manual implementation
    Junction *foo = std::find(std::begin(visited), std::end(visited), element);
    // When the element is not found, std::find returns the end of the range
    return (foo != std::end(visited));
  }*/
  
  void exitMaze(){
    fetch(maze->startPos);
    locomote(SOUTH);
  }
  
  void locomote(int dir){//[forward after turning?] 
    if(dir == currDir){
      forward(0);
    }
    else if(currDir == NORTH && dir == EAST){
      turnRight();
    }
    else if(currDir == EAST && dir == NORTH){
      turnLeft();
    }else if(dir-2==currDir || dir+2==currDir){
      back();
    }
    else{
      if(currDir<dir){
        turnRight();
      }else{
        turnLeft();
      }
    }
    updateCurrJunction(dir);
  }


  
  void turnRight(){
    if(currJunction==blind1 || currJunction==blind2){
      turnRightBlind();
    }else{
      turnRightCL();
    }
    currDir++;
    currDir = currDir%4;
  }
  void turnLeft(){
    if(currJunction==blind1 || currJunction==blind2){
      turnLeftBlind();
    }else{
      turnLeftCL();
    }
    if(currDir==0){
      currDir = 3;
    }
    else{
      currDir--;
    }
  }
  
  void updateCurrJunction(int dir){
    if(dir == EAST){
      currJunction = currJunction->eastConnection;
    }
    if(dir == WEST){
      currJunction = currJunction->westConnection;
    }
    if(dir == NORTH){
      currJunction = currJunction->northConnection;
    }
    if(dir == SOUTH){
      currJunction = currJunction->southConnection;
    }
  }

  void back(){
    turnRightCL();
    currDir = currDir+2;
    currDir = currDir%4;
  }

  

};
