#define max_serv 180
#define min_serv 0
#define off_serv 92
#define read_thresh 100

#define l 1
#define r 2
#define f 3
#define b 4

#include <Servo.h>
#include <Ultrasonic.h>
#include <QTRSensors.h>

Servo servoL; 
Servo servoR;
QTRSensors qtr;

const uint8_t SensorCount = 4;
uint16_t sensorValues[SensorCount];
int Sr;
int Sl;
int Srr;
int Sll;
int Sfront;

void setup() {
  servoL.attach(9);
  servoR.attach(10);

  pinMode(2,OUTPUT);
  digitalWrite(2,HIGH);
  // configure the sensors
  qtr.setTypeRC();
  qtr.setSensorPins((const uint8_t[]){3,4,5,6}, SensorCount);

  delay(500);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH); // turn on Arduino's LED to indicate we are in calibration mode

  // 2.5 ms RC read timeout (default) * 10 reads per calibrate() call
  // = ~25 ms per calibrate() call.
  // Call calibrate() 400 times to make calibration take about 10 seconds.
  for (uint16_t i = 0; i < 400; i++)
  {
    qtr.calibrate();
  }
  digitalWrite(LED_BUILTIN, LOW); // turn off Arduino's LED to indicate we are through with calibration
  delay(1000);
}

//----------------------Steering---------------------
void turnleft(int timeout){
  servoL.write(max_serv);
  servoR.write(max_serv);
  delay(timeout);
}

void turnright(int timeout){
  servoL.write(min_serv);
  servoR.write(min_serv);
  delay(timeout);
}

void swerveright(){
  servoL.write(min_serv);
  servoR.write(90);
}

void swerveleft(){
  servoL.write(90);
  servoR.write(max_serv);
}

void forward(int timeout){
  servoL.write(min_serv);
  servoR.write(max_serv);
  delay(timeout);
}

void back(int timeout){
  servoL.write(max_serv);
  servoR.write(min_serv);
  delay(timeout);
}

void stop(){
  servoL.write(90);
  servoR.write(90);
}

//----------------------Map solving---------------------

int nextOperation(){
  return 0;
}

//----------------------Operations-----------------------------
void linefollow(){
  int rightline = sensorValues[0];
  int leftline = sensorValues[1];
  
  //Logic
  if(rightline > read_thresh && leftline > read_thresh){
    forward(0);
  } else if(rightline > read_thresh && leftline < read_thresh) {
    swerveleft();
  } else if(rightline < read_thresh && leftline > read_thresh) {
    swerveright();
  } else {
    forward(0);
  }
}

void readSensor(){
  uint16_t position = qtr.readLineBlack(sensorValues);
  Sr = sensorValues[0];
  Sl = sensorValues[1];
  Srr = sensorValues[2];
  Sll = sensorValues[3];
}

void junction(){
  stop();
  if(Srr > read_thresh){
    turnright(100);
    do{
      readSensor();
    } while(Srr < read_thresh);
  } else if(Sll > read_thresh){
    do{
      readSensor();
    } while(Srr < read_thresh);
  }
}


void loop() {
  //Measurements
  //measureDistance();
  uint16_t position = qtr.readLineBlack(sensorValues);
  Sr = sensorValues[0];
  Sl = sensorValues[1];
  Srr = sensorValues[2];
  Sll = sensorValues[3];

  
  //Logic
  if(Srr < read_thresh && Sll < read_thresh){
    linefollow();
  } else {
    junction();
  }
  
  //Delay (if needed)
  delay(10);
}
