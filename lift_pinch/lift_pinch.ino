#include <Servo.h>

Servo lift;  // create servo object to control a servo
Servo pinch;
// twelve servo objects can be created on most boards

int pos = 0;    // variable to store the servo position

void setup() {
  lift.attach(10);
  pinch.attach(9);
  pinch.write(30);
}

void loop() {
  lift.write(0);
  delay(2000);
  pinch.write(30);
  delay(2000);
  pinch.write(10);
  delay(2000);
  lift.write(120);
  delay(2000);
  pinch.write(30);
  delay(2000);
}
