const int pingPin = 7; // Trigger Pin of Ultrasonic Sensor
const int echoPin = 4; // Echo Pin of Ultrasonic Sensor
long lastDuration = 0;

void setup() {
  pinMode(pingPin, OUTPUT);
  pinMode(echoPin, INPUT);
   Serial.begin(9600); // Starting Serial Terminal
}

long readSide(){
  long duration = 0;
  for(int i = 0; i<100; i++){
  digitalWrite(pingPin, LOW);
  //delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(2);
  digitalWrite(pingPin, LOW);
  duration += pulseIn(echoPin, HIGH);
  }
  return duration;
}

void loop() {
   long duration, inches, cm;
   duration = readSide();
   inches = microsecondsToInches(duration);
   cm = microsecondsToCentimeters(duration);
   Serial.print(inches);
   Serial.print("in, ");
   Serial.print(cm);
   Serial.print("cm , ");
   Serial.print(duration);
   Serial.print(" Microseconds");
   Serial.println();
   if (duration < lastDuration) {
    Serial.println("Stop the turn"); 
   }
   lastDuration = duration;
   delay(1500);
}

long microsecondsToInches(long microseconds) {
   return microseconds / 74 / 2;
}

long microsecondsToCentimeters(long microseconds) {
   return microseconds / 29 / 2;
}
