//WHICH TURNS ARE DEADENDS

#define max_serv 100
#define min_serv 80
#define off_serv 90
#define re 0
#define rl -1
#define l 1
#define r 2
#define f 3
#define bl 4
#define br 5
#define d 6
#define nbr_instruct 4//59

#include <Servo.h>
#include <QTRSensors.h>
#include <Ultrasonic.h>

Servo servoL; 
Servo servoR;
Servo pinch;
Servo lift;
QTRSensors qtr;

Ultrasonic ultrasonicleft(11, 12);
Ultrasonic ultrasonic(7, 4);
int distance;
int distanceleft;
int count = 21;
int nbr_cyl = 0;
int picktimeout = 0;

float yold = 0;
float I = 0;

const uint8_t SensorCount = 6;
uint16_t sensorValues[SensorCount];

volatile boolean flag = false;
volatile boolean turn_flag = true;

int instructions[nbr_instruct] = {br,r,r,r};//{r,l,l,r,d,l,l,br,r,r,l,l,d,l,l,r,d,l,f,l,l,r,l,l,d,r,r,l,f,r,rl,r,l,f,l,r,d,l,l,r,r,r,br,l,l,f,d,r,re,f,r,r,l,l,l,r,l,l,l};
volatile int pos = 0;

//--------------------ISRs-----------------------------
void pick_ISR(){
  if(nbr_cyl < 3)
    flag = true;
}

void button_ISR(){
  if(pos > 0){
    pos--;
  }
}

void setup() {
  //Moving
  servoL.attach(5);
  servoR.attach(6);

  //Picking
  lift.attach(10);
  pinch.attach(9);
  pinch.write(50);
  lift.write(0);
  attachInterrupt(digitalPinToInterrupt(3), pick_ISR, RISING);

  //Button for manual interference
  pinMode(2,INPUT);
  attachInterrupt(digitalPinToInterrupt(2), button_ISR, RISING);

  //Line sensors
  qtr.setTypeRC();
  qtr.setSensorPins((const uint8_t[]){A5,A4,A2,A3,A1,A0}, SensorCount);
  delay(500);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH); // turn on Arduino's LED to indicate we are in calibration mode

  // 2.5 ms RC read timeout (default) * 10 reads per calibrate() call
  // = ~25 ms per calibrate() call.
  // Call calibrate() 400 times to make calibration take about 10 seconds.
  for (uint16_t i = 0; i < 400; i++)
  {
    qtr.calibrate();
  }
  digitalWrite(LED_BUILTIN, LOW); // turn off Arduino's LED to indicate we are through with calibration

  // print the calibration minimum values measured when emitters were on
  Serial.begin(9600);
  
  for (uint8_t i = 0; i < SensorCount; i++)
  {
    Serial.print(qtr.calibrationOn.minimum[i]);
    Serial.print(' ');
  }
  Serial.println();

  // print the calibration maximum values measured when emitters were on
  for (uint8_t i = 0; i < SensorCount; i++)
  {
    Serial.print(qtr.calibrationOn.maximum[i]);
    Serial.print(' ');
  }
  Serial.println();
  Serial.println();
  delay(1000);
}

//---------------------Grasping-----------------------
void pick(){
  pinch.write(45);
}

void release(){
  pinch.write(65);
}

void picking(){
   if(flag && nbr_cyl < 2){
    servoL.detach();
    servoR.detach();
    pinch.write(20);
    delay(500);
    lift.write(120);
    delay(1000);
    pinch.write(50);
    delay(500);
    lift.write(0);
    delay(500);
    lift.write(120);
    delay(500);
    lift.write(0);
    flag = false;
    servoL.attach(5);
    servoR.attach(6);
    nbr_cyl++;
  } else if(flag){
    servoL.detach();
    servoR.detach();
    pinch.write(25);
    delay(500);
    lift.write(90);
    delay(100);
    flag = false;
    servoL.attach(5);
    servoR.attach(6);
    nbr_cyl++;
  }
}

//---------------Moving------------------
void turnleft(int timeout){
  servoL.write(min_serv);
  servoR.write(min_serv);
  delay(timeout);
}

void turnright(int timeout){
  servoL.write(max_serv);
  servoR.write(max_serv);
  delay(timeout);
}

void swerveright(){
  servoL.write(max_serv);
  servoR.write(90);
}

void swerveleft(){
  servoL.write(90);
  servoR.write(min_serv);
}

void forward(int timeout){
  servoL.write(max_serv);
  servoR.write(min_serv);
  delay(timeout);
}

void back(int timeout){
  servoL.write(max_serv);
  servoR.write(min_serv);
  delay(timeout);
}

//Used to turn until finding the line again
void turnrightCL(){
  turnright(0);
  delay(500);
  
  int rightline = sensorValues[1];
  int leftline = sensorValues[0];
  int sll = sensorValues[2];
  int srr = sensorValues[3];
  
  do {
  uint16_t position = qtr.readLineBlack(sensorValues);

  for (uint8_t i = 0; i < SensorCount; i++)
  {
    Serial.print(sensorValues[i]);
    Serial.print('\t');
  }
  Serial.println("hs");

  rightline = sensorValues[1];
  leftline = sensorValues[0];
  sll = sensorValues[2];
  srr = sensorValues[3];
  } while(leftline < 100 && rightline < 100);
}

void turnleftCL(){
  turnleft(0);
  delay(500);
  
  int rightline = sensorValues[1];
  int leftline = sensorValues[0];
  int sll = sensorValues[2];
  int srr = sensorValues[3];
  
  do {
  uint16_t position = qtr.readLineBlack(sensorValues);

  for (uint8_t i = 0; i < SensorCount; i++)
  {
    Serial.print(sensorValues[i]);
    Serial.print('\t');
  }
  Serial.println("hs");

  rightline = sensorValues[1];
  leftline = sensorValues[0];
  sll = sensorValues[2];
  srr = sensorValues[3];
  } while(leftline < 100 && rightline < 100);
}

//Used to turn when no line is present, might be changed for a open loop turn
boolean turnrightBlind(){   
    //Move from pos where it recognizes former wall (between 45 and 90 degrees)
    if(nbr_cyl > 2){
      turnright(850);//turnright(450);
    } else {
      turnright(800);
    }
    int distanceleft = 1000;
    int olddistance = 1500;
    
    //Turn until finding wall
    /*do {
    olddistance = distanceleft;
    turnright(20);
    servoR.detach();
    servoL.detach();
    delay(500);
    distanceleft = ultrasonicleft.read();
    distanceleft += ultrasonicleft.read();
    distanceleft += ultrasonicleft.read();
    distanceleft += ultrasonicleft.read();
    distanceleft += ultrasonicleft.read();
    distanceleft += ultrasonicleft.read();
    distanceleft += ultrasonicleft.read();
    distanceleft += ultrasonicleft.read();
    distanceleft += ultrasonicleft.read();
    distanceleft += ultrasonicleft.read();
    distanceleft += ultrasonicleft.read();
    distanceleft += ultrasonicleft.read();
    distanceleft += ultrasonicleft.read();
    distanceleft += ultrasonicleft.read();
    distanceleft += ultrasonicleft.read();
    Serial.print("Distance left is: ");
    Serial.println(distanceleft);
    servoL.attach(5);
    servoR.attach(6);
    } while(distanceleft < olddistance);
    turnleft(20);*/
    

  //Find line again
  int rightline = sensorValues[0]; //Could slap in extra sensors
  int leftline = sensorValues[1];
  int sll = sensorValues[2];
  int srr = sensorValues[3];
  int sr = sensorValues[4];
  int sl = sensorValues[5];
  //int sll2 = sensorValues[6];
  //int srr2 = sensorValues[7];

  do {
  picking();
  uint16_t position = qtr.readLineBlack(sensorValues);

  for (uint8_t i = 0; i < SensorCount; i++)
  {
    Serial.print(sensorValues[i]);
    Serial.print('\t');
  }
  Serial.println("hs");

  rightline = sensorValues[0];
  leftline = sensorValues[1];
  sll = sensorValues[3];
  srr = sensorValues[2];
  sr = sensorValues[4];
  sl = sensorValues[5];
  //sll2 = sensorValues[6];
  //srr2 = sensorValues[7];

  distance = ultrasonic.read();
  Serial.print("Distance is: ");
  Serial.println(distance);
  if(distance < 15){
     return false;
  } else {
  if(rightline > 100 || sr > 100) {
    swerveright();
  } else if(leftline > 100 || sl > 100){
    swerveleft();
  } else if (srr > 100) {
    turnright(200);
    forward(500);
    turnleft(100);
   } else if (sll > 100) {
    turnleft(200);
    forward(500);
    turnright(100);
  } else {
    forward(0);
  }
  }
  } while(leftline < 100 || rightline < 100); //Could be switched to and for easier switching?
  return true;
}

void turnleftBlind(){
    turnright(200);
    stop();
    distance = ultrasonic.read();
    Serial.print("Distance is: ");
    Serial.println(distance);
}

void stop(){
  servoL.write(90);
  servoR.write(90);
}

//-----------------------Logic-------------------------
void junctionHandling(){
  switch(instructions[pos]){
    case l:
    turnleftCL();
    break;

    case r:
    turnrightCL();
    break;

    case f:
    forward(200);
    break;

    case bl:
    turnleftBlind();
    break;

    case br:
    if(turnrightBlind()){
      count = 41;
      break;
    } else {
      pos++;
      turnleftCL(); //change to left for real maze
      break;
    }

    case d:
    turnrightCL();
    break;
    
    case re:
    if(nbr_cyl > 2){
      pos = 58;
    }
    turnrightCL();
    break;

    case rl:
    if(nbr_cyl > 1){
      pos = 51;
    }
    turnrightCL();
    break;
    
  }
  
  if(pos < nbr_instruct-1) {
    pos++;
  } else {
    pos = 0;
  }
}


void output(float u){
  if(u > 80.0){
    u = 80.0;
  } else if(u < -80.0){
    u = -80.0;
  }
  
  servoL.write(max_serv+(int)u);
  servoR.write(min_serv+(int)u);
}


void loop() {
  //Check if button is pressed
  //int val = digitalRead(2);
  //if(val != HIGH){
  digitalWrite(LED_BUILTIN, LOW);
  
  //Sensing
  uint16_t position = qtr.readLineBlack(sensorValues);

  for (uint8_t i = 0; i < SensorCount; i++)
  {
    Serial.print(sensorValues[i]);
    Serial.print('\t');
  }
  Serial.println("hs");

  //filter
  boolean onLine = false;
  
  for(int i = 0; i < SensorCount; i++){
    if(sensorValues[i] < 50)
      sensorValues[i] = 0;
    if(sensorValues[i] > 200)
      onLine = true;
  }
  
  float sr = (float)sensorValues[1];
  float sl = (float)sensorValues[0];
  float sll = (float)sensorValues[2];
  float srr = (float)sensorValues[3];
  float ssr = (float)sensorValues[4];
  float ssl = (float)sensorValues[5];
  
  
  
  //Pick
  picking();

  
  //Navigate
  float y = -sr - ssr*2 + sl + ssl*2;
  I = I + y;
  output(-(0.01*y + 0.02*(y-yold)));
  yold = y;
  

  //Make sure values don't go crazy
  if(count > 41)
    count = 41;
  
  //Delay

  //Block when keeping button pressed
  /*} else {
    servoL.write(90);
    servoR.write(90);
    digitalWrite(LED_BUILTIN, HIGH);
  }*/
}
