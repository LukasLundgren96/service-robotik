#include <Servo.h>
#include <QTRSensors.h>
#include <Ultrasonic.h>

#define max_serv 180
#define min_serv 0
#define off_serv 90
#define re 0
#define rl -1
#define l 1
#define r 2
#define f 3
#define bl 4
#define br 5
#define d 6
#define nbr_instruct 59

//Servo Motors
Servo servoL;
Servo servoR;
Servo pinch;
Servo lift;

//Line sensing
QTRSensors qtr;
const uint8_t SensorCount = 6;
uint16_t sensorValues[SensorCount];
int rightline = sensorValues[1];
int leftline = sensorValues[0];
int sll = sensorValues[2];
int srr = sensorValues[3];
int sr = sensorValues[4];
int sl = sensorValues[5];

//Distance sensing
Ultrasonic ultrasonicleft(11, 12);
Ultrasonic ultrasonic(7, 4);
int distance;
int distanceleft;

//Picking
int count = 21;
int nbr_cyl = 0;
volatile boolean flag = false;

//Instructions
int instructions[nbr_instruct] = {r, l, l, r, d, l, l, br, r, r, l, l, d, l, l, r, d, l, f, l, l, r, l, l, d, r, r, l, f, r, rl, r, l, f, l, r, d, l, l, r, r, r, br, l, l, f, d, r, re, f, r, r, l, l, l, r, l, l, l};
volatile int pos = 0;

//--------------------ISRs-----------------------------
void pick_ISR() {
  if (nbr_cyl < 3)
    flag = true;
}

void button_ISR() {
  if (pos > 0)
    pos--;
}


//--------------------Setup------------------------------
void setup() {
  //Moving
  servoL.attach(5);
  servoR.attach(6);

  //Picking
  lift.attach(10);
  pinch.attach(9);
  pinch.write(50);
  lift.write(0);
  attachInterrupt(digitalPinToInterrupt(3), pick_ISR, RISING);

  //Button for manual interference
  pinMode(2, INPUT);
  attachInterrupt(digitalPinToInterrupt(2), button_ISR, RISING);

  //Line sensors
  qtr.setTypeRC();
  qtr.setSensorPins((const uint8_t[]) {
    A5, A4, A2, A3, A1, A0
  }, SensorCount);
  delay(500);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH); // turn on Arduino's LED to indicate we are in calibration mode
  for (int i = 0; i < 400; i++)
  {
    qtr.calibrate();
  }
  digitalWrite(LED_BUILTIN, LOW);
}

//----------------------Sensing-----------------------

void lineSens() {
  qtr.readLineBlack(sensorValues);
  int rightline = sensorValues[1];
  int leftline = sensorValues[0];
  int sll = sensorValues[2];
  int srr = sensorValues[3];
  int sr = sensorValues[4];
  int sl = sensorValues[5];
}


//---------------------Grasping-----------------------
void pick() {
  pinch.write(45);
}

void release() {
  pinch.write(65);
}

void picking() {
  if (flag && nbr_cyl < 2) {
    //Sequence for placing on roof
    servoL.detach();
    servoR.detach();
    pinch.write(20);
    delay(500);
    lift.write(120);
    delay(1000);
    pinch.write(50);
    delay(500);
    lift.write(0);
    delay(500);
    lift.write(120);
    delay(500);
    lift.write(0);
    flag = false;
    servoL.attach(5);
    servoR.attach(6);
    nbr_cyl++;
  } else if (flag) {
    //Sequence for holding in gripper
    servoL.detach();
    servoR.detach();
    pinch.write(25);
    delay(500);
    lift.write(90);
    delay(100);
    flag = false;
    servoL.attach(5);
    servoR.attach(6);
    nbr_cyl++;
  }
}

//---------------Moving------------------
void turnleft(int timeout) {
  servoL.write(max_serv);
  servoR.write(max_serv);
  delay(timeout);
}

void turnright(int timeout) {
  servoL.write(min_serv);
  servoR.write(min_serv);
  delay(timeout);
}

void swerveright() {
  servoL.write(min_serv);
  servoR.write(90);
}

void swerveleft() {
  servoL.write(90);
  servoR.write(max_serv);
}

void forward(int timeout) {
  servoL.write(min_serv);
  servoR.write(max_serv);
  delay(timeout);
}

//Used to turn until finding the line again
void turnrightCL() {
  //Open loop to get off the line
  turnright(500);

  //Turn untill finding line again with one of the sensors
  do {
    lineSens()
  } while (leftline < 100 && rightline < 100);
}

void turnleftCL() {
  //Open loop to get off the line
  turnleft(500);

  //Turn untill finding line again with one of the sensors
  do {
    lineSens()
  } while (leftline < 100 && rightline < 100);
}

boolean findLine() {
  do {
    picking();
    lineSens();

    distance = ultrasonic.read();
    if (distance < 15) {
      return false;
    } else {
      if (rightline > 100 || sr > 100) {
        swerveright();
      } else if (leftline > 100 || sl > 100) {
        swerveleft();
      } else if (srr > 100) {
        turnright(200);
        forward(500);
        turnleft(100);
      } else if (sll > 100) {
        turnleft(200);
        forward(500);
        turnright(100);
      } else {
        forward(0);
      }
    }
  } while (leftline < 100 || rightline < 100); //Could be switched to and for easier switching?
  return true;
}

boolean turnrightBlind() {
  //Move from pos where it recognizes former wall (between 45 and 90 degrees)
  if (nbr_cyl > 2) {
    turnright(850);
  } else {
    turnright(800);
  }

  //Find line again
  return findLine();
}

void turnleftBlind() {
  //Move from pos where it recognizes former wall (between 45 and 90 degrees)
  if (nbr_cyl > 2) {
    turnright(850);
  } else {
    turnright(800);
  }

  //Find line again
  return findLine();
}

//-----------------------Logic-------------------------
void junctionHandling() {
  //Execute next instruction in instruction set
  switch (instructions[pos]) {
    case l:
      turnleftCL();
      break;

    case r:
      turnrightCL();
      break;

    case f:
      forward(200);
      break;

    case bl:
      turnleftBlind();
      break;

    case br:
      if (turnrightBlind()) {
        count = 41;
        break;
      } else {
        pos++;
        turnleftCL(); //If misses hard turn
        break;
      }

    case d:
      turnrightCL();
      break;

    case re: //Junction before escape point 1
      if (nbr_cyl > 2) {
        pos = 58;
      }
      turnrightCL();
      break;

    case rl: //Junction before escape point 2
      if (nbr_cyl > 1) {
        pos = 51;
      }
      turnrightCL();
      break;

  }

  //Increment position in instruction set
  if (pos < nbr_instruct - 1) {
    pos++;
  } else {
    pos = 0;
  }
}


void loop() {
  //Sensing
  sensLine();

  //Pick
  picking();

  //Move
  if ((sll > 100 || srr > 100) && (count > 40)) {
    count = 0;
    junctionHandling();
  } else {
    count++;
    if (rightline > 100 && leftline > 100) {
      forward(0);
    } else if (rightline > 100 && leftline < 100) { //Can remove checking left line
      swerveleft();
    } else if (rightline < 100 && leftline > 100) {
      swerveright();
    } else if (sr > 100) {
      swerveleft();
    } else if (sl > 100) {
      swerveright();
    } else {
      distance = ultrasonic.read();
      if (distance < 15 && instructions[pos] > f) {
        count = 0;
        junctionHandling();
      } else {
        forward(1);
      }
    }
  }

  //Make sure values don't go crazy
  if (count > 41)
    count = 41;
}
