#define max_serv 180
#define min_serv 0
#define off_serv 92

#include <Servo.h>
#include <Ultrasonic.h>

Ultrasonic ultrasonic(12, 13);
Servo servoL; 
Servo servoR;
Servo pinch;

int pos = 0;    // variable to store the servo position
int distance;

void setup() {
  servoL.attach(9);
  servoR.attach(10);
  lift.attach(11);
}

void turnleft(int timeout){
  servoL.write(max_serv);
  servoR.write(max_serv);
  delay(timeout);
}

void turnright(int timeout){
  servoL.write(min_serv);
  servoR.write(min_serv);
  delay(timeout);
}

void forward(int timeout){
  servoL.write(min_serv);
  servoR.write(max_serv);
  delay(timeout);
}

void loop() {
  distance = ultrasonic.read();
  if(distance > 40){
    forward(1000);
  } else {
    turnleft(1000);
  }
}
