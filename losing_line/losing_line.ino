#define max_serv 180
#define min_serv 0
#define off_serv 90
#define l 1 
#define r 2
#define f 3
#define bl 4
#define br 5
#define nbr_instruct 2

#include <Servo.h>
#include <QTRSensors.h>
#include <Ultrasonic.h>

Servo servoL; 
Servo servoR;
Servo pinch;
Servo lift;
QTRSensors qtr;

Ultrasonic ultrasonic(11, 12);
Ultrasonic ultrasonicleft(7, 4);
int distance;
int distanceleft;
int count = 21;

const uint8_t SensorCount = 6;
uint16_t sensorValues[SensorCount];

volatile boolean flag = false;
volatile boolean turn_flag = true;

int instructions[nbr_instruct] = {r,br};//{r,l,f,r,l,l,f,l,l,l,l,l};//{r,l,f,r,r,r,l,r,r,l,l,l,l,r,r,l,l,l,r,l}; 20
int pos = 0;


void pick_ISR(){
  flag = true;
}

void setup() {
  //Moving
  servoL.attach(5);
  servoR.attach(6);

  //Picking
  lift.attach(10);
  pinch.attach(9);
  pinch.write(30);
  lift.write(0);
  attachInterrupt(digitalPinToInterrupt(3), pick_ISR, RISING);

  //Line sensors
  qtr.setTypeRC();
  qtr.setSensorPins((const uint8_t[]){A5,A4,A3,A2,A1,A0}, SensorCount);
  delay(500);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH); // turn on Arduino's LED to indicate we are in calibration mode

  // 2.5 ms RC read timeout (default) * 10 reads per calibrate() call
  // = ~25 ms per calibrate() call.
  // Call calibrate() 400 times to make calibration take about 10 seconds.
  for (uint16_t i = 0; i < 400; i++)
  {
    qtr.calibrate();
  }
  digitalWrite(LED_BUILTIN, LOW); // turn off Arduino's LED to indicate we are through with calibration

  // print the calibration minimum values measured when emitters were on
  Serial.begin(9600);
  
  for (uint8_t i = 0; i < SensorCount; i++)
  {
    Serial.print(qtr.calibrationOn.minimum[i]);
    Serial.print(' ');
  }
  Serial.println();

  // print the calibration maximum values measured when emitters were on
  for (uint8_t i = 0; i < SensorCount; i++)
  {
    Serial.print(qtr.calibrationOn.maximum[i]);
    Serial.print(' ');
  }
  Serial.println();
  Serial.println();
  delay(1000);
}


//---------------Moving------------------
void turnleft(int timeout){
  servoL.write(max_serv);
  servoR.write(max_serv);
  delay(timeout);
}

void turnright(int timeout){
  servoL.write(min_serv);
  servoR.write(min_serv);
  delay(timeout);
}

void swerveright(){
  servoL.write(min_serv);
  servoR.write(90);
}

void swerveleft(){
  servoL.write(90);
  servoR.write(max_serv);
}

void forward(int timeout){
  servoL.write(min_serv);
  servoR.write(max_serv);
  delay(timeout);
}

void back(int timeout){
  servoL.write(max_serv);
  servoR.write(min_serv);
  delay(timeout);
}

void turnrightCL(){
  servoL.write(40);
  servoR.write(40);
  delay(500);
  
  int rightline = sensorValues[1];
  int leftline = sensorValues[0];
  int sll = sensorValues[2];
  int srr = sensorValues[3];
  
  do {
  uint16_t position = qtr.readLineBlack(sensorValues);

  for (uint8_t i = 0; i < SensorCount; i++)
  {
    Serial.print(sensorValues[i]);
    Serial.print('\t');
  }
  Serial.println("hs");

  rightline = sensorValues[1];
  leftline = sensorValues[0];
  sll = sensorValues[2];
  srr = sensorValues[3];
  } while(leftline < 100 || rightline < 100);
}

void turnleftCL(){
  servoL.write(140);
  servoR.write(140);
  delay(500);
  
  int rightline = sensorValues[1];
  int leftline = sensorValues[0];
  int sll = sensorValues[2];
  int srr = sensorValues[3];
  
  do {
  uint16_t position = qtr.readLineBlack(sensorValues);

  for (uint8_t i = 0; i < SensorCount; i++)
  {
    Serial.print(sensorValues[i]);
    Serial.print('\t');
  }
  Serial.println("hs");

  rightline = sensorValues[1];
  leftline = sensorValues[0];
  sll = sensorValues[2];
  srr = sensorValues[3];
  } while(leftline < 100 || rightline < 100);
}

void turnrightBlind(){
    do {
    servoL.detach();
    servoR.detach();
    distance = ultrasonic.read();
    Serial.print("Distance is: ");
    Serial.println(distance);
    servoL.attach(5);
    servoR.attach(6);
    } while(distance > 10);
    
    //Move from pos where it recognizes former wall
    turnright(200);
    int distanceleft = 1000;
    
    //Turn until finding wall
    do {
    turnright(50);
    servoR.detach();
    servoL.detach();
    distanceleft = ultrasonicleft.read();
    Serial.print("Distance left is: ");
    Serial.println(distanceleft);
    servoL.attach(5);
    servoR.attach(6);
    } while(distanceleft > 7);

    //Find line again
  int rightline = sensorValues[0];
  int leftline = sensorValues[1];
  int sll = sensorValues[2];
  int srr = sensorValues[3];
  
  do {
  uint16_t position = qtr.readLineBlack(sensorValues);

  for (uint8_t i = 0; i < SensorCount; i++)
  {
    Serial.print(sensorValues[i]);
    Serial.print('\t');
  }
  Serial.println("hs");

  rightline = sensorValues[0];
  leftline = sensorValues[1];
  sll = sensorValues[3];
  srr = sensorValues[2];

  if(rightline > 100) {
    swerveright();
  } else if(leftline > 100){
    swerveleft();
  } else if (srr > 300) {
    turnright(200);
    forward(500);
    turnleft(100);
   } else if (sll > 300) {
    turnleft(200);
    forward(500);
    turnright(100);
  } else {
    forward(0);
  }
  } while(leftline < 100 || rightline < 100);
}

void turnleftBlind(){
    turnright(200);
    stop();
    distance = ultrasonic.read();
    Serial.print("Distance is: ");
    Serial.println(distance);
}

void stop(){
  servoL.write(90);
  servoR.write(90);
}

//---------------------Grasping-----------------------
void pick(){
  pinch.write(45);
}

void release(){
  pinch.write(65);
}

//
void junctionHandling(){
  switch(instructions[pos]){
    case l:
    turnleftCL();
    break;

    case r:
    turnrightCL();
    break;

    case f:
    forward(200);
    break;

    case bl:
    turnleftBlind();
    break;

    case br:
    turnrightBlind();
    break;
  }
  
  if(pos < nbr_instruct-1) {
    pos++;
  } else {
    pos = 0;
  }
}


void loop() {
  //Sensing
  
  uint16_t position = qtr.readLineBlack(sensorValues);

  for (uint8_t i = 0; i < SensorCount; i++)
  {
    Serial.print(sensorValues[i]);
    Serial.print('\t');
  }
  Serial.println("hs");

  int rightline = sensorValues[1];
  int leftline = sensorValues[0];
  int sll = sensorValues[2];
  int srr = sensorValues[3];
  int backlineleft = sensorValues[4];
  int backlineright = sensorValues[5];

  //Pick
  if(flag){
    servoL.detach();
    servoR.detach();
    pinch.write(10);
    lift.write(120);
    delay(1000);
    pinch.write(30);
    delay(500);
    lift.write(0);
    flag = false;
    servoL.attach(5);
    servoR.attach(6);
  }

  
  //Navigate
  if((sll > 300 || srr > 300) && (count > 40)){ //((sll > 300 && srr > 300) && (leftline > 100 && rightline > 100))
    count = 0;
    junctionHandling();
  } else {
    count++;
   if(rightline > 100 && leftline > 100){
      forward(0);
  } else if(rightline > 100 && leftline < 100) {
    swerveleft();
  } else if(rightline < 100 && leftline > 100) {
    swerveright();
  } else {
    servoL.detach();
    servoR.detach();
    distance = ultrasonic.read();
    Serial.print("Distance is: ");
    Serial.println(distance);
    servoL.attach(5);
    servoR.attach(6);
    if(distance < 10){
      count=0;
      junctionHandling();
    } else {
      forward(0);
    }
    /*if(count > 40){
      count = 0;
      junctionHandling();
    } else {
      count++;
      forward(0);
    }*/
  }
  }
  
  //Delay
}
