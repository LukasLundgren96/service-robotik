#define max_serv 180
#define min_serv 0
#define off_serv 92

#include <Servo.h>
#include <Ultrasonic.h>

Ultrasonic ultrasonic(12, 13);
Servo servoL; 
Servo servoR;
Servo pinch;

int distances[10];
int pos = 0;
int distance;
int val = 0;

void setup() {
  servoL.attach(9);
  servoR.attach(10);
  pinch.attach(11);
}

void turnleft(int timeout){
  servoL.write(max_serv);
  servoR.write(max_serv);
  delay(timeout);
}

void turnright(int timeout){
  servoL.write(min_serv);
  servoR.write(min_serv);
  delay(timeout);
}

void forward(int timeout){
  servoL.write(min_serv);
  servoR.write(max_serv);
  delay(timeout);
}

void back(int timeout){
  servoL.write(max_serv);
  servoR.write(min_serv);
  delay(timeout);
}

void stop(){
  servoL.write(90);
  servoR.write(90);
}

void pick(){
  pinch.write(45);
}

void release(){
  pinch.write(65);
}

void measureDistance(){
  distance = ultrasonic.read();

  if(pos < 10) {
    distances[pos] = distance;
    pos++;
  } else {
    pos=0;
    distances[pos] = distance;
  }
  
  distance = 0;
  for(int i = 0; i < 10; i++)
    distance += distances[i];
  distance/10;
}

void loop() {
  //Measure
  //measureDistance();
  distance = ultrasonic.read();
  
  //Logic
  if(distance > 6){
    release();
    forward(0);
  } else {
    delay(100);
    stop();
    pick();
    turnright(1000);
    forward(1000);
    release();
    back(1000);
    turnleft(1000);
  }

  //Delay
  delay(50);
}
